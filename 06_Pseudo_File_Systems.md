## Pseudo File Systems
A pseudo file system maintains information about the currently running system. This information does not persist across reboots.   
It exists whilst the system is running and only in RAM.

- `/sys`: <https://www.kernel.org/doc/Documentation/filesystems/sysfs.txt>
- ``/proc``: <https://www.kernel.org/doc/Documentation/filesystems/proc.txt>
- `/dev`:
- `/proc/interrupts`:

Let's go system folder:  
`$ cd /sys`  

Let's go block folder under system folder:  
`$ cd block`  

Let's go sda folder under block folder:  
`$ cd sda`  

To learn size of sda folder:  
`$ cat size`  

Let's go dev folder:  
`$ cd /dev`  

List all folders starting with sda keyword:  
`$ ls sda*`

Let's go process folder:  
`$ cd /proc`  

Let's list interrrupts:  
`$ cat interrupts`  

Let's see first 10 interrupts:  
`$ head -n 10`  

To watch every 1 second top 10 interrupt:  
`$ watch -n 1 head -n 10 interrupts`
