## Shared Software Management
Most programs will make use of shared libraries or modules to prevent the need of code duplication.  
Dll in Windows: .so or .ko in Linux  
so: Standard modules  
ko: Kernel modules, or drivers that the kernel is going to use.  

To show dependencies of a module:  
`$ ldd /bin/ls`: List loaded kernel modules  
`$ lsmod`: List loaded kernel modules  
`$ /etc/ld.so/conf`: Maintains PATH to search for modules  

For example, following programs uses libc.so.6  
so that keeps the programs smaller and development time shorter:
- `ls`
- `grep`
- `cat`

`$ dpkg -l geany`: Show files of package  
`$ ldd /usr/bin/geany`: List loaded kernel modules  
Let's see what is in the PATH file:  
`$ echo $PATH`

Let's check mouse process:  
`$ lsmod | grep psmouse`: List loaded kernel modules and grep psmouse module  
`$ grep psmouse /proc/modules`: Grep psmouse module from /proc/modules directory  

When user is root:  
- `$ modprobe -r psmouse`
- `$ modprobe -v psmouse`  
These two commands will send: insmod (load a kernel module).
Here is the command line output:  
`insmod /lib/modules/4.9.0-8-amd64/kernel/drivers/input/mouse/psmouse.ko `  

Let's display description of psmouse:  
`$ modinfo -d psmouse`  
> birol@capa:~/Desktop/test$ modinfo -d psmouse  
PS/2 mouse driver

