## POSIX Compliant Regular Expressions
`$ grep cpU /proc/cpuinfo`: Search "cpU" in the file. This search is case sensitive and no result is found.  
`$ grep -i cpU /proc/cpuinfo`: Search "cpU" in the file using -i (ignore case) option.  

### Search For a Text
`$ grep '\bcpu\b' /proc/cpuInfo`: Search 'cpu' word in the file.  

### Search For Word at Start of Line
If we would like to see the lines that beginning with 'cpu', then we can use `^` to represent the start of the line.
`$ grep '^cpu\b'`

### Remove Commented and Blank Lines
We can use ^# to represent lines begin with comment.  
Empty lines can be shown with ^$, (lines begin with a CR).  
The option -v reverses our search.
The option -e allows more than one expression.

`$ grep -ve'^#' -ve'^$' /etc/crontab`: The output is now free of commented lines and empty lines.

### Permanently Remove Those Lines
The stream editor `sed` can be used to edit a file.  
Let's build a command piece by piece:  
- sed: Edit stream for 
- begins with: /^
- comment: #
- end search string: /
- delete: d
- separate expressions: ;
- begins with: /^
- empty lines: $
- end search string: /
- delete: d  

`$ sed '/^#/d;/^$/d' /etc/crontab`  
`$ sed -i '/^#/d;/^$/d' /etc/crontab`: Updates in place.

### More Complex Expressions
using grep with option -E or egrep, we can look for more complex expressions.
- grep -E: Look for a regular expression
- [A-Z]{1, 2}: 1 or 2 upper case letters
- [0-9]{1, 2}: 1 or 2 digits
- [A-Z]?: ? is used for optional. So it means optional upper case letter
- \s: Single space
- [0-9]: Single number
- [A-Z]{2}: 2 upper case character

`$ grep -E '[A-Z]{1,2}[0-9]{1,2}[A-Z]?\s[0-9][A-Z]{2}' postcode`  
`$ grep -E '[A-Z]{5}' crontab`