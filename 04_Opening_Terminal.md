## Opening Terminal
The terminal is an interface in which I can type and execute text based commands.[1]

In Debian, 
- Under GNOME: `Applications> System Tools> Terminal` or
- the keyboard shortcut `Alt + F2` for `Run Application` and type `gnome-terminal`[1]

To create a shortcut for this [2]
- `Applications> Settings> Keyboard`
- Find `Custom Shortcuts`
- Click on the `+` button
- Specify the name and command for your shortcut (e.g. "Terminal" and gnome-terminal)
- click on `Add`, the new shortcut appears in the list
- click on `Disabled` to the right of the shortcut, and define the shortcut you want to use.

Sources  
[1] <https://askubuntu.com/a/38220>  
[2] <https://unix.stackexchange.com/a/268462/321852>  

In my case, I use 
- `Ctrl + Alt + T`: Shortcut to open terminal.
- `Ctrl + D`: Exit terminal.
- `Ctrl + L`: Clear screen of terminal.
- `Ctrl + Shift + Plus`: Make terminal font bigger (depends on distro).
- `Ctrl + Shift + Minus`: Make terminal font smaller (depends on distro).