## Shutting down
- `$ shutdown`  
- `$ shutdown -r now`  
- `$ shutdown -h+5 "System will be closed in 5 minutes"`
- `$ shutdown -c`: Cancels a pending event

`$ jobs`: Show active jobs.  
To run a command in the background, put `&` end of the command. For example:  
`shutdown -h+5 "System will be shutdown in 5 minutes" &`