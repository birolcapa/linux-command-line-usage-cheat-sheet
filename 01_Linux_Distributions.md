## Linux Distributions
Linux is the kernel version.

A Linux distribution (often abbreviated as distro) is an operating system made from a software collection, which is based upon the Linux kernel and, often, a package management system.[1]

Linux users usually obtain their operating system by downloading one of the Linux distributions, which are available for a wide variety of systems ranging from embedded devices (for example, OpenWrt) and personal computers (for example, Linux Mint) to powerful supercomputers (for example, Rocks Cluster Distribution).[1]

A distribution (or distro, for short), in the context of this website, refers to a specific implementation of a GNU/Linux operating system. A project which ships an operating system that includes the Linux kernel is said to be a Linux distribution.[2] 

Sources:  
[1] <https://en.wikipedia.org/wiki/Linux_distribution>  
[2] <https://distrowatch.com/dwres.php?resource=glossary#distribution>