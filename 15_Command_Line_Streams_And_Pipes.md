## Command Line Streams and Pipes
Before a command is executed, its input and output may be redirected using a special notation. To see more details about redirection:
`$ man bash`: Search for "REDIRECTION"  

`$ ... > ...`: Write to a file.  
`$ ... >> ...`: Append to a file.  
`$ set -o noclobber`: Prevents writing a file that already exist.  
`$ set +o noclobber`: Does not prevent writing a file that already exist.  
`$ man bash > bash_usage`: Writes manual of bash to bash_usage file.  
`$ man ls > bash_usage`: Since noclobber is set, command fails:
> birol@capa:~/Desktop/test$ man ls > bash_usage  
bash: bash_usage: cannot overwrite existing file  

To ignore noclobber, use >|:  
`man ls >| bash_usage`: Writes manual of ls to bash_usage file (ignoring noclobber)  

### Pipelines
`$ man bash`: Search for "Pipelines"  
A pipeline is a sequence of one or more commands seperated by one of the control operators `|` or `&`.  
`|`: Unnamed pipeline  
`mkfifo`:  make FIFOs(named pipes)

### Redirecting Standard Output
`$ ls /etc > file1.txt`: Redirect standard output to `file1.txt`  
`$ ls /etc 1> file1.txt`: More correct redirection with parameter `1`  
`$ ls /etc 1>> file1.txt`: Append to file1 with parameter `1`  

The value `1` as the output identifier is optional as the default output is standard output  

### Redirecting Standard Error
`$ man unknown 2> file1.txt`: Redirect standard error to `file1.txt`  
`$ man unknown 2 >> file1.txt`: Append to file  
`$ man unknown > file1.txt 2>&1`: Redirect standard error and output to same file. The value 2 identifies that we are redirecting errors, &1 says same place as 1.  
`$ ls /home /hime > file1.txt 2>&1`: Redirect standard error and output to same file.  
> birol@capa:~/Desktop/test$ ls /home/ /hime 1> file1.txt 2>&1  
birol@capa:~/Desktop/test$ cat file1.txt  
ls: cannot access '/hime': No such file or directory  
/home/:  
birol  

### Read Standard Input
`$ ... < ...`: Read from file.  
Let's report file system disk usage in a human readable format:  
`$ df -h > diskfree.txt`  
`$ cat < diskfree.txt`  

### Pipes
Let's find definition of a pipeline:  
`$ man bash`: Search for "Pipelines"  
A pipeline is a sequence of one or more commands seperated by one of the control operators `|` or `|&`.  
The standard output of command is connected via a pipe to standard input of another command:  
`$ command | another_command`  

`$ ls -l | wc -l`: List directory contents using long losting format, then send output to word count.  In other words: Count the number of lines returned by ls command.  
> birol@capa:~$ ls -l | wc -l  
13

It is possible to create named pipes:  
`$ mkfifo mypipe`: Named pipes are files.

Let's redirect to/from mypipe:  
Open terminal 1, and run: `ls -l > mypipe`  
Open terminal 2, and run: `wc -l < mypipe`  
With these commands, two separate process bash shell users can communicate through this pipe file.

### Command tee
`tee` command is used to send output to a file and to the screen at the same time:  

from pipeline  
-> to file  
-> to screen  
	

`$ ls /etc > newfile`: Contents are not seen in the screen.  
Let's see the ouput also in the screen:  
`$ which tee`  
`$ ls /etc | tee myfile`