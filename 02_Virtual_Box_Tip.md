## VirtualBox Tip
When I created a Debian distro in my virtual machine using a VirtualBox, I run following commands to make it more useful in terms of screen size. [1, 2]  
`$ sudo apt update`  
`$ sudo apt install build-essential module-assistant dkms`  
`$ sudo m-a prepare`  
`$ sudo sh /media/cdrom/VBoxLinuxAdditions.run`  
Sources:  
[1] <https://www.neontribe.co.uk/debian-virtualbox-guest-additions/>  
[2] <https://unix.stackexchange.com/a/268462/321852>  

## Distro Types
There are many distros.  
- Enterprise Level Support:
    - SUSE Enterprise Linux
    - Red Hat Enterprise Linux
- Small or Median Business:
    - OpenSuse
    - Fedora
    - CentOS
- Home:
    - Ubuntu

<https://distrowatch.com> may give ideas about the distributions.  
Lubuntu is another minimal distro. Lubuntu means Ubuntu with LXDE (light-weight desktop environment).  
Raspberry Pi's Raspbian: It is a debian based distribution
