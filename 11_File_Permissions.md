## File Permissions
There are multiple commands for editing file permissions:
- `chmod`: Change file mode bits
- `chown`: Change file owner and group
- `chgrp`: Change group ownership
- `stat`: Display file or file system status
- `umask`: Set file mode creation mask
Let's see permissions convention:  

read | write | execution| result
--|--|--|--|
1|1|1 |= 7
1|1|0 |= 6 
1|0|0 |= 4

<br>

user |group |others|
--|--|--|
RWX  |RW -  |R - -
7    |6     |4 

`$ pwd`: Print working directory  
`$ touch file1`: Update the access and modification times of each FILE (file1 for that case) to the current time.  

`$ ls -l file1`: Shows permissions. For that case it was:  
> -rw-r--r-- 1 birol birol 0 Dec 11 10:52 file1

`$ stat file1`: Display file or file system status. For that case it was:  

>  File: file1  
  Size: 0         	Blocks: 0          IO Block: 4096   regular empty file  
Device: 801h/2049d	Inode: 948962      Links: 1  
Access: (0644/-rw-r--r--)  Uid: ( 1000/   birol)   Gid: ( 1000/   birol)  
Access: 2018-12-11 10:52:02.097219696 +0000  
Modify: 2018-12-11 10:52:02.097219696 +0000  
Change: 2018-12-11 10:52:02.097219696 +0000  
 Birth: -  


`$ umask`: Display or set file mode mask. In other terms, shows default file permissions.  

`$ umask 0`: Make all permissions zero  
`$ umask 077`: Make only the user has rights to file3  

`$ ls -ld`: Show permissions of the directory  
> drwxr-xr-x 2 birol birol 4096 Dec 11 10:52 

`chmod 777 file1`: Change mode file1 to 777. For that case it was:  
> -rwxrwxrwx 1 birol birol 0 Dec 11 10:52 file1

Let's create file2:  
`$ touch file2`  
Let's check its permissions  
`$ ls -l`  
> -rw-r--r-- 1 birol birol 0 Dec 11 10:58 file2

Let's change its permissions  
Change mode of file2 for user as rwx, group as rwx, others rwx  
`$ chmod u=rwx, g=rwx, o=rwx file2`  

Let's create file3:
`$ touch file3`
Let's check its permissions:
`$ ls -l`
>total 0  
-rwxrwxrwx 1 birol birol 0 Dec 11 10:52 file1  
-rwxrwxrwx 1 birol birol 0 Dec 11 10:58 file2  
-rw-r--r-- 1 birol birol 0 Dec 11 12:12 file3  

`$ chmod +rx file3`: Give permission of read and execute for everyone  
Then it will become  
> `$ ls -l`  
-rwxr-xr-x 1 birol birol 0 Dec 11 12:12 file3