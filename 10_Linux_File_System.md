## Linux File System
`$ lsblk`: List block devices  
`$ lsblk /dev/sda`  
`$ lsblk -l /dev/sda`  
`$ fdisk`: Manipulate disk partition table  
`$ fdisk -l`: List partition tables for specidied devices  
`$ parted /dev/sda print`: Display the partition table for /dev/sda  
`mkfs`: Format file system  
`mount`: Show mounted file systems  
`umount /dev/sdb1`: Umount a directory or a device  
`blkid /dev/sdb1`: Blok id of the file system  

An example:  
`$ mount -t ext4 /dev/sdb1`: Mount sdb1  
`$ mount -t ext4 /dev/sdb1 /mnt`: Mount sdb1 as mnt  
`$ mount -t ext4 -o acl,noexec /dev/sdb1 /mnt`: Mount sdb1 as mnt with extra options  
`$ mount | grep mnt`: Check if it is mounted  
`$ umount /mnt`: Unmount it  