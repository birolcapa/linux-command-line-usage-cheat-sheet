## Getting Distro Info
To get more detail information about my distro:  
`$ uname -r`: Report the kernel release.  
`$ uname -a`: Reports all information.  
`$ lsb_release -a`: This command often lists the distribution. 