## Determine Hardware Settings
Physical consoles are numbered tty1-tty6  
Pseudo terminals represent logical terminals such as GUI or X-Terminals and those made from remote connections such as SSh or telnet.  
`$ tty`: Identify connected terminal.  
`$ who` or `$w`: Identify all connected terminals.  
`uptime`: How long the machine have been up?  
> *12:25:06 up  4:17,  4 users,  load average: 0.00, 0.00, 0.00*  

`$ which update`: Show if uptime is in my search path. It is not in my current directory  
> /usr/bin/uptime


`$ echo $PATH`: Show search path.  
> /usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games

`$ free`: How much memory is being used?  
`$ free -m`: How much memory is being used in megabytes?  
`$ free --help`: shows options of free command.  
`$ lsusb`: What is connected to machine via usb bus?  
`$ uname`: Display the operating system.  
`$ uname -m`: Display machine platform.  
`$ uname -r`: Display version of Linux kernel  
`$ hwinfo`: Show all hardware information.  
`$ hwinfo --netcard`: Show network cards.  