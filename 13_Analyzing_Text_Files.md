## Analyzing Text Files
`$ head`: Print  the  first  10 lines of each FILE to standard output.  
`$ tail`: Print  the  last  10  lines of each FILE to standard output.  
`$ sort`: Sort lines of text files. Organize data in columns
More: More is a filter for paging through text one screenful at a time.  
`$ less`: Less is a program similar to "more", but it has many  more  features. Less  does  not  have to read the entire input file before starting, so with large input files it starts up faster than text  editors  like  vi.   
`$ cut`: Print selected parts of lines from each FILE to standard output.  
`$ cat`: Comes from contacanate. Concatenate files and print on the standard output.  
`$ tac` - concatenate and print files in reverse  

Let's create three files under Desktop/test dir:  
`$ mkdir test; cd test`  
`$ touch file1; vi file1`  
`$ touch file2; vi file2`  
`$ touch file3; vi file3`  
Let's see files:  
`$ cat file1`  
`$ cat file1 file2`  
`$ tac file3`: Shows file bottom to up  
>This is line3 of file3  
This is line2 of file3  
This is line1 of file3  

`$ cat -vet file3`: Shows hidden chars in the file.  
>This is line1 of file3$  
This is line2 of file3$  
This is line3 of file3$  

Let's create an sh file:  
`$ touch test.sh; vi test.sh;`  
Here is the inside of the file:
>cat file1  
cat file2  
tac file3  

Let's change mode of the test.sh file, so it becomes executable:  
`$ chmod +x test.sh`  
Since we are in this directory, we can run it by typing ./
`$ ./test.sh`
Here is the result:
> This is line1  
This is line1 of file2  
This is line2 of file2  
This is line3 of file3  
This is line2 of file3  
This is line1 of file3  

Let's analyze come different files in our system:  
`$ cd /proc`: Go to process directory  
`$ ls`: List all processes here  
`$ cat version`: See version of your Linux  
> birol@capa:/proc$ cat version 
Linux version 4.9.0-8-amd64 (debian-kernel@lists.debian.org) (gcc version 6.3.0 20170516 (Debian 6.3.0-18+deb9u1) ) #1 SMP Debian 4.9.130-2 (2018-10-27)

`$ cat mounts`: See all mounted files  
`$ cat /proc/sys/net/ipv4/ip_forward`: See if there is any ip forwarding  

Let's analyze another big file in our system:  
`$ head /etc/passwd`: First 10 lines of passwd file  
`$ tail /etc/passwd`: Last 10 lines of passwd file  
`$ tail -n 1 /etc/passwd`: Last 1 line of passwd file  
`$ tail -1 /etc/passwd`: Last 1 line of passwd file  
`$ tail -f /var/log/messages`: This command will leave the file open  

`$ wc -l /etc/services`: Shows line numbers of the file (word count)  
If we run following command after this command:  
`$ more !$`: That means 'more /etc/services'  
`$ less !$`: That means 'less /etc/services'  
To quit from less, you shall type :q  

Let's list long list format of a big directory.  
Following commands are very handy in that:  
`$ ls -l /etc | more`  
`$ ls -l /etc | less`  

cut and sort
`$ cut`: Remove sections from each line of files
`$ sort`: Sort lines of text files

Let's see latest 3 items of passwd file:
`$ tail -n 3 /etc/passwd`
> Debian-gdm:x:117:123:Gnome Display Manager:/var/lib/gdm3:/bin/false
birol:x:1000:1000:birol,,,:/home/birol:/bin/bash
vboxadd:x:999:1::/var/run/vboxadd:/bin/false

It is delimited by ':'  
if I want to get field 1 and field 3 which are seperated by ':'  
Following command may help:  
`$ cut -f1,3, -d ":" !$`: This command is actually means  
`$ cut -f1,3 -d ":" /etc/passwd`: Get field 1 nd 3 which are seperated by `:`
> root:0  
daemon:1  
bin:2  

`$ sort /etc/passwd`: Sort lines of file  
`$ sort -r /etc/passwd`: Sort lines of file reversely  
`$ sort -k1 -t":" /etc/passwd`: Sort file depending column 1 with seperator `:`  
`$ sort -r -k1 -t":" /etc/passwd`: Sort file depending column 1 with seperator `:` reversely  
`$ cut -f2 -d":" /etc/passwd | sort -u`: Cut field 4 by seperating with delimiter `:` and send the output to sort. By using `sort -u`, get only unique lines and sort them.  
`$ sort -k4 -n -t":" /etc/passwd`: Sort numerically column 4 of /etc/passwd with delimiter ":"  
`$ cut -f4 -d":" /etc/passwd | sort -n -u`: Cut field 4 by seperating with delimiter `:` and send the output to sort. By using `sort -n -u`, get only unique lines and sort them numerically:  
>0  
1  
2  


