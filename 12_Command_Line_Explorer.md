## Command Line Explorer

`$ stat . `: Show stats of a directory  
`$ ls`: List files  
`$ ls -a`: List all files including hidden files  

Let's go to home dir:
`$ cd /home/birol`  
Let's see bash history:  
`$ cat .bash_history`: Show history of bash  

`!cat`: Means run the latest command again  
In that case this means run `$ cat .bash_history` again  
Moreover:  
- `$!g`: Run the last command starting with g
- `$!c`: Run the last command starting with c

Let's go /etc dir:  
`$ cd /etc`  
Come back to home again:  
`$ cd` 
Let's go /usr dir:   
`$ cd /usr`  
`$ !cd`: would just repeat `$ cd /usr`.  
If I want a different thing, I could write:  
`$ !?etc`: This means  `$ cd /etc`  
`?` is used for taking parameter!

Let's go to Desktop directory:  
`$ cd Desktop`  
Let's create a test directory:
`$ mkdir test`  
When I type:  
`$ cd !$`: Means cd to last argument which means cd to test directory

`CTRL + r`: Search in the history

`$ history`: Show all command lines history

`$ history -c`: Clean history  
`$ history -r`: Read history  

If you want to really clear the history:
- `$ history -c`
- `$ history -w`: writes history files to nothing  
