## Basic File Management
`$ cp`: Copy files and directories.  
`$ mv`: Move (rename) files.  
`$ rm`: Remove files of directories.  
`$ ls`: List directory contents.   
`$ tar`: An archiving utiliy.  
`$ find`: Search for files in a directory hierarchy.  

Following 3 options may be helpful:  
- i: Interactive, prompt before action.  
- R: Recursion.
- a: Keep original permissions.

Let's create three files under Desktop/test dir:  
`$ mkdir test; cd test`  
`$ touch file1; vi file1`  
`$ touch file2; vi file2`  
`$ touch file3; vi file3`  
Let's copy file1 to file5:
`$ cp file1 file5`
> birol@capa:~/Desktop/test$ cp file1 file5  
birol@capa:~/Desktop/test$ ls  
file1  file2  file3  file5

Let's move (rename) file5 as file6:  
`$ mv file5 file6`
> birol@capa:~/Desktop/test$ mv file5 file6  
birol@capa:~/Desktop/test$ ls  
file1  file2  file3  file6

Let's move file6 to parent directory:  
`$ mv file6 ..`
Let's copy file1 to parent directory:  
`$ cp file1 ..`

Let's do more copy interactively:  
`cp -i file2 ..`: Copy file2 to parent directory interactively  
`cp - i file* ..`: Copy file* to parent directory interactively  

`file*`: Files followed by 0 or any amount of characters
`file?`: Files followed by a single character

Let's make a backup directory:
`$ mkdir backup`
Copy test directory to backup directory:
`$ cp -R files/ backup/`
> birol@capa:~/Desktop$ ls  
backup  test

Let's see directories and subdirectories of backup:  
`$ ls -R backup/`
> birol@capa:~/Desktop$ ls -R backup/  
backup/:  
test  
backup/test:  
file1  file2  file3  test.sh  

Let's see details of file1:
`$ ls -l backup/test/file1`  
> birol@capa:~/Desktop$ ls -l backup/test/file1   s
-rw-r--r-- 1 birol birol 14 Feb 17 11:04 backup/test/file1

To preserve the file permissions and ownerships, we need to copy in archive mode:  
`$ cp -a file1 file101`
> birol@capa:~/Desktop/test$ ls -l file1  
-rw-r--r-- 1 birol birol 14 Dec 15 06:05 file1  
birol@capa:~/Desktop/test$ cp -a file1 file101  
birol@capa:~/Desktop/test$ ls -l file101   
-rw-r--r-- 1 birol birol 14 Dec 15 06:05 file101  

Let's remove files and directories:  
`$ rm file1`: Remove a file1  
`$ rm -i file2`: Remove a file2 interactively  
`$ rm -f f*`: Remove files starting with f and followed by any number of characters  
`$ rm -f f?`: Remove files starting with f and followed by one letter  

`$ rmdir backup/`: If directory is empty, it can be deleted.
If directory is not empty, then it cannot be deleted.
> birol@capa:~/Desktop$ rmdir backup/  
rmdir: failed to remove 'backup/': Directory not empty

To do so, rescursive switch can be used:  
`$ rm -rf backup/`: Remove with recursive(r) force(f) options.

### Directory Listing
`$ ls -l file1`: List information about `file1` with long listing format.  
`$ ls -lh file1`:  List information about `file1` with human readable long listing format.  
`$ ls -ld test/`: List information about directory `test` itself, not their contents.  
> birol@capa:~/Desktop$ ls -ld test/  
drwxr-xr-x 2 birol birol 4096 Feb 17 11:10 test/  
|-> This d means it is a directory

`$ ls --color=auto /home/birol`: List information with default coloring.

We can create an alias for this command as:  
`$ alias ls='ls --color=auto'`  
Then whenever we run following command:  
`$ ls /home/birol`  
It will actually call:  
`$ ls --color=auto /home/birol`  

`$ alias`: List the aliases.
> birol@capa:~/Desktop$ alias --help  
alias: alias [-p] [name[=value] ... ]  
    Define or display aliases.  
    ...

> birol@capa:~/Desktop$ alias  
alias ls='ls --color=auto'  

If we want to run regarding command without alias:  
For example, if I want to run `ls` command without `color` option as the original one:  
`$ \ls`: Runs the original `ls` command which means there will be no coloring.  

`$ ls -l`: List information about current directory with long listing format.  
> birol@capa:~/Desktop$ ls -l  
total 4  
drwxr-xr-x 2 birol birol 4096 Feb 17 11:10 test  

dir|user perm|group perm|perm for eveyone| number of links through to file |named user|group user|
---|---|---|---| --- |---|---|
d|rwx|r-x|r-x| 2| birol| birol|  

Let's work on a very big directory:  
`$ ls /etc`: A very long listing  
`$ ls -t /etc`: List according to last modified time  
`$ ls -lt /etc`: Long list according to last modified time  
`$ ls -lrt /etc`: Reverse long list according to last modified time  
`$ ls -lrt /etc | less`: Get a reverse long list according to last modified time and send it to less  

Let's list disks:
`$ fdisk -l`: List  the  partition  tables  for the specified devices and then exit.  

Let's use archiving utility:  
`$ tar -cvf etc.tar /etc`: Creates etc.tar file using create, verbose, file options.

To see regarding sizes:
`$ du -sh /etc`: Disk usage with size human readable format of etc directory.
`$ du -sh etc.tar`
> birol@capa:~/Desktop$ sudo du -sh /etc/  
6.8M	/etc/  
birol@capa:~/Desktop$ du -sh etc.tar  
4.0M	etc.tar  

`$ tar -czvf etc.tgz /etc`: Create etc.tgz with create, gzip, verbose, file options.

`$ ls -lh etc*`: Long list with human readable option
> birol@capa:~/Desktop$ ls -lh etc*
-rw-r--r-- 1 birol birol 4.0M Feb 17 20:21 etc.tar
-rw-r--r-- 1 birol birol 940K Feb 17 20:25 etc.tgz

Let's see how much time each archiving algorithm takes.
`$ time tar -cvf etc.tar /etc`  
`$ time tar -cvzf etc.gz /etc`  
`$ time tar -cjvf etc.bz2 /etc`  

To determine file type:  
`$ file etc.tar`: Determine file type.  
> birol@capa:~/Desktop$ file etc.tar   
etc.tar: POSIX tar archive (GNU)

`$ tar -tzf etc.tgz`: See inside of a tar file with test, zip, file options.

`$ tar -xzvf etc.tgz`: Expand out of a tar file using expand, zipperd, verbose, file options.

Let's use find command:  
`$ find . -newer file1`: Find files from this directory which are newer than file1.  
`$ find -newer file1 -print`: Find files from this directory which are newer than file1.  
`$ find -newer etc.tar -delete`: Find files from this directory which are newer than `etc.tar` and delete them.  
`$ find -newer etc.tar -type f -delete`: Find files from this directory which are newer than `etc.tar` and which are files and then delete them.  
`$ find /usr/share -name '*.pdf'`: Find pdf files under that directory.  
`$ find /usr/share -maxdepth 1 -name '*.pdf'`: Find pdf files under that directory using max depth is as 1 (1 means 1 directory below).  
`$ find /usr/share -maxdepth 3 -name '*.pdf'`: Find pdf files under that directory using max depth is as 3 (3 means 3 directory below).  
`$ find /usr/share -maxdepth 3 -name '*.pdf' -exec ls -lh {} \;`: Find and then run `ls -lh` for these files

`$ find /usr/share -maxdepth 3 -size +138k`: Find all files size bigger than 138k  
`$ find /usr/share -maxdepth 3 -size -138k`: Find all files size smaller than 138k  

