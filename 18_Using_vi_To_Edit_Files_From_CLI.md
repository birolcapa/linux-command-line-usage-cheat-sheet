## Using vi To Edit Files From CLI
vi is just links or is aliased to vim.  
vim is improved vi.  
It is the most common editor used in Linux.  

It is a modal editor.  
Command: When we enter vim  
Insert/Edit: When we are editing text. ESC to leave.  
Last line mode: Enter from command mode using `:`

To open a file: `$ vi newfile`  
Enter last line with: `:`  
Quit with: `q`  
Quit without any changes: `!q`  

When you are in command mode:  
Use insert command: `i`  
Enter text.  
Return from command mode with: `ESC`  
Enter last line with: `:`  
Exit using: `x`  
Save the changes (write) and quit: `wq`  

Insert at cursor position: `i`  
Insert at start of current line: `I`  
Append to cursor position: `a`  
Append to end of current line: `A`  
Insert new line below current position: `o`  
Insert new line above current position: `O`

Delete current character using: `x`  
Delete word: `dw`  
Undo the change: `u`

### Line Navigation in vi
In command mode use `7G` to go to line 7.  
To move backwards one word: `w`  
To move backwards one word: `b`  
End of line: `$`  
Start of line: `^`  

If we would like to edit a file at a given place:  
`$ vi +127 /etc/services`  
`$ vi line_number filename`  
`$ vi +/codaauth2 /etc/services`  
`$ vi keyword filename`  

In the command mode, type:  
`603G`: Go line number 603G.  
`1G`: Go start of file  
`G`: Go end of file  

In the command mode, type:  
`w`: 1 word forward  
`b`: 1 word backward  
`5w`: 5 word forward  
`3b`: 3 word backward  

### Options
From last line mode  
`set number`  
`set nonumber`  
`set invnumber`  
`set nohlsearch`  
`syntax on`

### Read Into vim
We can read files or command output into the current file.  
In last line mode: `: r address.txt`  
From last line mode: `: r! blkid /dev/sda2`  

### Write to Another File
We can write to files as well.  
From last line mode:
`: w out.txt`  
`: 23,37w /tmp/file`: Lines between 23 and 37 would be written to /tmp/file.

Let's open a new file:  
- `$ vi newfile`  
- Then enter command mode: `:r /etc/hosts`  
So the text in the hosts file will be appended to new file.  
- `:set number`: See the line numbers  
- In the command mode, write: `:2,3w ip.txt`  
We can create an ip.txt by using line 2 and line 3
- `:x` exit
- Then `$ vi ip.txt`  

We will see that we got the host info.
If we want to add new info to this file:
- `r! id`
- `r! hostname`

### Search and Replace
We can use % to specify the whole document or range 103, 188.  
From last line mode, to replace "Inc" with "Ltd":  
`:%s/Inc/Ltd`

Let's open our services file:  
`$ vi services`  
Let's replace IANA with cabbage:  
In the command line mode:  
`%s/IANA/cabbage`  
Then let's search for cabbage word:  
In the command line mode:  
`/cabbage`  
While in the search mode, you can use `n` keyword for the next found word.

We can just search for a couple of lines:  
`:1,20s/cabbage/IANA`

We may add four spaces to some lines:  
`:14, 20s/^/    /`

### Persist Changes
The system wide configuration: /etc/vimrc  
Personal configuration: ~/.vimrc (. means hidden file)

### Mappings and Settings
We can map macros to keys:  
- nmap <C-N>: set invnumber<CR> (Control + N)
- nmap <CR> G
set number nohlsearch showmode