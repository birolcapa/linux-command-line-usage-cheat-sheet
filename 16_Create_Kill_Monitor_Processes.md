## Create Kill and Monitor Processes
`$ bg`: Moves jobs to the background.  
`$ fg`: Move job to the foreground.  
`$ jobs`: Display status of jobs.  
`$ kill`: Send a signal to a job.

`$ ps`: Report a snapshot of the current processes.  
`$ pgrep`: Grep processes based on name and other attributes.  
`$ pkill`: Signal processes based on name and other attributes.  
`$ killall`: Kill all processes.  

`$ uptime`: Tell how long the system is running.  
`$ top`: Display Linux processes.  

## The procps Family
The tools that we will use come from the package procps.  
On Debian based system we can list the contents of a package:  
`$ dpkg -L procps` 

`$ which ps`:
> birol@capa:~/Desktop/test$ which ps  
/bin/ps

`$ dpkg -S /bin/ps`:
> birol@capa:~/Desktop/test$ dpkg -S /bin/ps  
procps: /bin/ps

or we can use:  
`$ dpkg -S $(which ps)`
`$ dpkg -L procps`

`uptime` reads:
- /proc/uptime: How long system up and how long idle
- /proc/loadavg: Load averages and number of processes running

`$ uptime`:  
> birol@capa:~/Desktop/test$ uptime  
 14:53:06 up  6:02,  1 user,  load average: 0.79, 0.69, 0.65  

`$ cat /proc/uptime`
> birol@capa:~/Desktop/test$ cat /proc/uptime   
21779.62 17737.88  
uptime in seconds | idle in seconds

`$ cat proc/loadavg`
> birol@capa:~/Desktop/test$ cat /procbg/loadavg  
0.60 0.66 0.64 2/364 2974
last minute | last 5 minute| last 15 minute| 2/364: 364 active processes
2 of which is running | last process id to be issued

### Managing Jobs
To automatically run commands in the background, & is added.
Let's run a sleeping command:  
`$ sleep 180`  
So we will wait for 180 seconds.  
If we want out terminal back: Press `ctrl + z`  

Let's run a sleeping command at the background by using & suffix:  
`$ sleep 180 &`

Let's check it with different commands:  
`$ bg`
> birol@capa:~$ bg  
[1]+ sleep 180 &

`$ ps`
> birol@capa:~$ ps  
  PID TTY          TIME CMD  
 2027 pts/0    00:00:00 bash  
 2032 pts/0    00:00:00 sleep  
 2035 pts/0    00:00:00 ps  

`$ jobs`  
Before 180 seconds:  
> birol@capa:~$ jobs  
[1]+  Running                 sleep 180 &

After 180 seconds:  
> birol@capa:~$ jobs  
[1]+  Done                    sleep 180  

We can bring the job foreground by using:  
`$ fg job_number`

Let's run a background job:  
> birol@capa:~$ sleep 190 &  
[1] 2042

Let's bring it to the foreground:  
> birol@capa:~$ fg 1  
sleep 190  

### Managing processes:
`$ ps -l`: Report processes in long list format.  
`$ ps -f`: Report processes in full listing format.  
`$ ps -ef`: Report every process.  
`$ ps -ef | grep nginx`: Find nginx process.  
`$ pgrep nginx`: Grep prosess nginx.  

Let's sleep at the background:  
`$ sleep 900 &`  
Then find that process:  
`$ pgrep sleep`

> birol@capa:~$ sleep 900&  
[1] 2112  
birol@capa:~$ pgrep sleep  
2112  

Then kill that process by writing:  
`$ kill 2112`

Let's run 3 sleep processes:  
`$ !sleep`  
`$ !sleep`  
`$ !sleep`  
Then kill all those processes:  
> birol@capa:~$ killall sleep  
[1]   Terminated              sleep 900  
[2]-  Terminated              sleep 900  
[3]+  Terminated              sleep 900  

`kill` actually sends a signal to a job.  
`kill -l` shows those signals:  
> 1) SIGHUP	 2) SIGINT	 3) SIGQUIT	 4) SIGILL	 5) SIGTRAP
> 6) SIGABRT	 7) SIGBUS	 8) SIGFPE	 9) SIGKILL	10) SIGUSR1  
> 11) SIGSEGV	12) SIGUSR2	13) SIGPIPE	14) SIGALRM	15) SIGTERM
> ...

If we really kill a process like a bash:  
`$ kill -9 process_id`

The default signal for kill is TERM.  

### Using top

`$ top`: Display Linux processes.  Press q to quit from it.  
`$ renice`: Alter priority of running processes.  
sort and display

`$ top -n 1`: Run top once, display and then quit.  
`$ top -n 2 -d 3`: Make 2 captures from top with a delay of 3 seconds.

While running `top` command:  
- l: for the load
- t: for the tasks
- M: for the memory
can be used to switch on off options.

By default the list is listed according to CPU priority.