## Software and Package Management
For Debian and Ubuntu:
.deb files and dpkg software management
DPKG database located at `/var/lib/dpkg`  
`$ dpkg -l`: List all debs installed on device  
`$ dpkg -i nameof.deb`: Install deb to system  
`$ dpkg -r nameofpackage`: Remove deb from system  
`$ dpkg -reconfigure packagename `: Re-run config wizard if available  
`$ apt-get install packagename`: Install a package

Let's use `vim-tiny` package to run following commands:  
`dpkg -l vim-tiny`: List package  
`dpkg -L vim-tiny`: List files  
`dpkg -s vim-tiny`: -s, --status package-name...: Report status of specified package.  
`dpkg -S vim-tiny`: -S, --search filename-search-pattern...: Search for a filename from installed packages.


`$ apt-get update`: Update local cache  
`$ ls /var/cache/apt`  
`$ ls /var/cache/apt/archives`  

`$ apt-get install vim`

`$ apt-chache search postfix`  
`$ apt-chache search pkgnames postfix`  
`$ dpkg -reconfigure packagename`  

The online repositories are configured under:  
`$ vi /etc/apt/sources.list`